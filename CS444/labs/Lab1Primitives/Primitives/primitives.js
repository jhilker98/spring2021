

var canvas;
var gl;

var numVertices  = 32;


var pointsArray = [
// Nose Cone
vec4(3, 8, 0.5, 1.0),//0
vec4(3.5, 9.5, 0.5, 1.0), //1
vec4(4, 8, 0.5, 1.0), //2

// left body
vec4(4, 1, 0.5, 1.0), //3 Triangle Strip
vec4(4, 8, 0.5, 1.0), //4
vec4(3, 8, 0.5, 1.0), //5

//body - right
vec4(3.0, 1, 0.5, 1.0), //6
vec4(4, 1, 0.5, 1.0),// 7
vec4(3, 8, 0.5, 1.0), //8

/// top left fin
vec4(3, 7, 0.5, 1.0), //9
vec4(2, 6, 0.5, 1.0), //10
vec4(2, 5.8, 0.5, 1.0), //11
vec4(2, 5.6, 0.5, 1.0), //12
vec4(3.2, 5.6, 0.5, 1.0), // 13

// top right fin
vec4(4, 7, 0.5, 1.0), //14
vec4(5, 6, 0.5, 1.0), //15
vec4(5, 5.8, 0.5, 1.0), //16
vec4(5, 5.6, 0.5, 1.0), //17
vec4(4, 5.6, 0.5, 1.0), //18

// bottom left fin
vec4( 3.0, 1,  0.5, 1.0), //19
vec4( 3.0, 2, 0.5, 1.0), //20
vec4( 2, 1, 0.5, 1.0), //21

// bottom right fin
vec4( 5, 1,  0.5, 1.0), //22
vec4( 4, 2, 0.5, 1.0), //23
vec4( 4, 1, 0.5, 1.0), //24

// flames

vec4(3.5, 1, 0.5, 1.0), //25 Triangle Strip
vec4(3, 1, 0.5, 1.0), //26
vec4(3, 0, 0.5, 1.0), //27
vec4(3, 0, 0.5, 1), //28
vec4(3.5, 1.2, 0.5, 1), //29
vec4(4, 0, 0.5, 1), //30

vec4(4, 1, 0.5, 1.0), //31
vec4(4, 0, 0.5, 1.0), //32
vec4(3.5, 1, 0.5, 1.0), //33

// stars
vec4(3.35, 1, 0.5, 1), //34
vec4(3.34, 1.01, 0.5, 1), //35
vec4(3.37, 1.02, 0.5, 1), //36
];

var colorsArray = [
// nose cone
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),

//body - right
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),

// body - left 
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),

// top left fin
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),

//    // top right fin
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),
vec4(0.47, 0.47, 0.47, 1),


//bottom left fin
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),

// bottom right fin
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),
vec4(0.43, 0.43, 0.43, 1),

// flames
vec4(1, 0.5, 0.1, 1),
vec4(1, 0.5, 0.1, 1),
vec4(0.84, 0.36, 0.05, 1),

vec4(1, 0.5, 0.1, 1),
vec4(1, 0.5, 0.1, 1),
vec4(0.84, 0.36, 0.05, 1),

vec4(1, 0.5, 0.1, 1),
vec4(1, 0.5, 0.1, 1),
vec4(0.84, 0.36, 0.05, 1),

// stars
vec4(1.0, 1, 1, 1),
vec4(1.0, 1, 1, 1),
vec4(1.0, 1, 1, 1), 
    ];

var near = -1;
var far = 1;
var radius = 1.0;
var theta  = 0.0;
var phi    = 0.0;
var dr = 5.0 * Math.PI/180.0;

var left = 0;
var right = 6;
var ytop = 10;
var bottom = 0;


var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;
var eye;

const at = vec3(0.0, 0.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);


window.onload = function init() {
    canvas = document.getElementById( "gl-canvas" );
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    
    //gl.clearColor( 1.0, 1.0, 1.0, 1.0 );
    gl.clearColor(0.01, 0.01, 0.01, 1);
    gl.enable(gl.DEPTH_TEST);

    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW );
    
    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor);

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW );
    
    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
 
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

// buttons to change viewing parameters

    document.getElementById("Button1").onclick = function(){left  -= 1; right += 1;};
    document.getElementById("Button2").onclick = function(){left  += 1; right -= 1;};
    document.getElementById("Button3").onclick = function(){bottom  -= 1; top += 1;};
    document.getElementById("Button4").onclick = function(){bottom  += 1; top -= 1;};
    document.getElementById("Button5").onclick = function(){left  -= 1; right += 1; bottom  -= 1; top += 1;};
    document.getElementById("Button6").onclick = function(){left  += 1; right -= 1; bottom  += 1; top -= 1;;};
       
    render();
}


var render = function() {
        gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            
        eye = vec3(0, 0, 1);

        modelViewMatrix = lookAt(eye, at , up); 
        projectionMatrix = ortho(left, right, bottom, ytop, near, far);
        
        gl.uniformMatrix4fv( modelViewMatrixLoc, false, flatten(modelViewMatrix) );
        gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );

        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.drawArrays(gl.TRIANGLE_STRIP, 3, 3);
        gl.drawArrays(gl.TRIANGLE_STRIP, 6, 3);
        gl.drawArrays(gl.TRIANGLE_FAN, 9,5);
        gl.drawArrays(gl.TRIANGLE_FAN, 13,6);
        gl.drawArrays(gl.TRIANGLES, 19,3);
        gl.drawArrays(gl.TRIANGLES, 22,3);
        gl.drawArrays(gl.TRIANGLES, 25,3);
        gl.drawArrays(gl.TRIANGLE_STRIP, 28,3);
        gl.drawArrays(gl.TRIANGLE_STRIP, 31,3);
        gl.drawArrays(gl.LINES, 34,3);
	requestAnimFrame(render);
    }