// Jacob Hilker

"use strict";

var canvas;
var gl;



var points = [    
    vec4(0, 3.5, 0.5, 1), //diamond
    vec4(0.5, 3, 0.5, 1), //diamond
    vec4(0, 2.5, 0.5, 1), //diamond
    vec4(-0.5, 3, 0.5, 1), //diamond
    


    vec4(-1.5, -1, 0.5, 1), //door
    vec4(-1.5, 1, 0.5, 1), //door
    vec4(1.5, 1, 0.5, 1), //door
    vec4(1.5, -1, 0.5, 1), //door


    vec4(-1, 1, 0.5, 1), //window
    vec4(-1, 0, 0.5, 1), //window
    vec4(1, 0, 0.5, 1), //window
    vec4(1, 1, 0.5, 1), //window

    vec4(-4, 3, 0.5, 1), //roof
    vec4(0, 4, 0.5, 1), //roof
    vec4(4, 3, 0.5, 1), //roof

    vec4(0, 4, 0.5, 1), // roof right clear
    vec4(4, 4, 0.5, 1), //roof right clear
    vec4(4, 3, 0.5, 1), //roof right clear

    vec4(0, 4, 0.5, 1), // roof left clear
    vec4(-4, 4, 0.5, 1), //roof left clear
    vec4(-4, 3, 0.5, 1), //roof left clear

];

var colors = [
    vec4(.80, 0.14, 0.11, 1), //diamond
    vec4(.80, 0.14, 0.11, 1), //diamond
    vec4(.80, 0.14, 0.11, 1), //diamond
    vec4(.80, 0.14, 0.11, 1), //diamond

    vec4(0.69, 0.38, 0.53, 1), //door
    vec4(0.69, 0.38, 0.53, 1), //door
    vec4(0.69, 0.38, 0.53, 1), //door
    vec4(0.69, 0.38, 0.53, 1), //door


    vec4(0.27, 0.52, 0.53, 1), //window
    vec4(0.27, 0.52, 0.53, 1), //window
    vec4(0.27, 0.52, 0.53, 1), //window
    vec4(0.27, 0.52, 0.53, 1), //window

    vec4(0.11, 0.13, 0.13, 1), //roof
    vec4(0.11, 0.13, 0.13, 1), //roof
    vec4(0.11, 0.13, 0.13, 1), //roof


    vec4(1, 1, 1, 1), //roof right clear
    vec4(1, 1, 1, 1), //roof right clear
    vec4(1, 1, 1, 1), //roof right clear
    vec4(1, 1, 1, 1), //roof left clear
    vec4(1, 1, 1, 1), //roof left clear
    vec4(1, 1, 1, 1), //roof left clear

];

var numVertices  = 8;

// Shader transformation matrices
var modelViewMatrix, projectionMatrix;
var modelViewMatrixLoc, projectionMatrixLoc;

var eye, at, up;

var number=1;

var theta=0;
var theta2=0;

var down=true;
var ty=0;

window.onload = function init()
{
    canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 0.11, 0.13, 0.13, 1.0 );

    gl.enable(gl.DEPTH_TEST);
	
    at = vec3(0.0, 0.0, 0.0);
    up = vec3(0.0, 1.0, 0.0);
    eye = vec3(0.0, 0.0, 1.5);

    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

	//Create your color buffer
    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );

	//Create your vertex buffer
    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );


    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );
	
	//Model and Projection Buffers
    modelViewMatrixLoc = gl.getUniformLocation( program, "modelViewMatrix" );
    projectionMatrixLoc = gl.getUniformLocation( program, "projectionMatrix" );

	//Set up Ortho Projections
    projectionMatrix = ortho(-4, 4, 0, 4, 1, -1);
    gl.uniformMatrix4fv( projectionMatrixLoc, false, flatten(projectionMatrix) );


    render();
}

function drawHouse(){
    modelViewMatrix = mat4();
    modelViewMatrix = lookAt(eye, at, up);
    //modelViewMatrix = mult(modelViewMatrix, translate(-0.01, 0.9, 0));
    gl.uniformMatrix4fv(modelViewMatrixLoc, false, flatten(modelViewMatrix));
    gl.drawArrays(gl.TRIANGLES, 12, 3);
    gl.drawArrays(gl.TRIANGLES, 15, 6);
}
function drawWindows(){
    modelViewMatrix = mat4(); //go back to 0,0,0
    modelViewMatrix = lookAt(eye,at,up); //set up our camera
    var matrix3 = mult(modelViewMatrix,scalem(.5,.5,1));
    matrix3 = mult(matrix3, translate(4.1, 4.6, 1));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3)); //mult from 0,0,0
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
    
    modelViewMatrix = mat4(); //go back to 0,0,0
    modelViewMatrix = lookAt(eye,at,up); //set up our camera
    modelViewMatrix= mult(modelViewMatrix, translate(-2.9,2.4,0));
    var matrix3 = mult(modelViewMatrix,scalem(.53,.46,1));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3)); //mult from 0,0,0
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);

    modelViewMatrix = mat4(); //go back to 0,0,0
    modelViewMatrix = lookAt(eye,at,up); //set up our camera
    modelViewMatrix= mult(modelViewMatrix, translate(-3,1,0));
    var matrix3 = mult(modelViewMatrix,scalem(.9,.8,1));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3)); //mult from 0,0,0
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);

    modelViewMatrix = mat4(); //go back to 0,0,0
    modelViewMatrix = lookAt(eye,at,up); //set up our camera
    modelViewMatrix= mult(modelViewMatrix, translate(2.5,0.5,0));
    var matrix3 = mult(modelViewMatrix,scalem(.55,.35,1));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3)); //mult from 0,0,0
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
		
} 

function drawEntrance(){ 
    if(down==true){ //make it go right
           ty = ty -.01; //controls the speed the larger the number the faster
        if(ty <= -0.8){
            down=false;
        }
    }
    if(down==false){ //makes it go left
        ty = ty +.01;
        if(ty >= 0.3){
            down=true;
        }
    }
    modelViewMatrix = mat4();
    modelViewMatrix = lookAt(eye,at,up);
    var matrix3 = mult(modelViewMatrix,translate(0,ty,0));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3));
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
}
    
    
    

function drawDiamond(){
   theta = (theta - 2) % 360;
    modelViewMatrix = mat4();
    modelViewMatrix = lookAt(eye, at, up);
    var matrix3 = mult(modelViewMatrix, translate(0, 3, 0)); 
    matrix3 = mult(matrix3, rotateZ(theta));
    matrix3 = mult(matrix3, translate(0, -3, 0));
    gl.uniformMatrix4fv(modelViewMatrixLoc,false,flatten(matrix3));
    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
}
function render()
{
	gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	modelViewMatrix = lookAt(eye, at, up);

    drawDiamond();
	drawEntrance();
	drawWindows();
	drawHouse();
  	window.requestAnimationFrame(render);

}
