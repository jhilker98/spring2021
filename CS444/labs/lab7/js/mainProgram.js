var camera;
var scene;
var renderer;
var geo, mat;
var planet, planetAmb, sun, moon, sunLight, sunLightHelper, star, starLight;
var sat, satlight, sat2, junk;

const loader = new THREE.TextureLoader();
const objects = [];

function init() {
  // Initializes a scene.
  camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  scene = new THREE.Scene();
  //scene.background = new THREE.Color(0x2e3440);
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  document.addEventListener('keydown', onDocumentKeyDown);
  camera.position.z = 10;
}

function drawPlanet() {
  // Draws the icy planet and moon in the center of the screen
  geo = new THREE.SphereGeometry(2.3, 32, 32);
  mat = new THREE.MeshPhongMaterial({
    color: 0xb9ffef,
    shininess: -1,
    //color: 0xff0000,
    map: loader.load('assets/icy.png'),
  });

  planet = new THREE.Mesh(geo, mat);
  planet.scale.set(1.25, 1.25, 1.25);
  scene.add(planet);
  objects.push(planet);
  planet.position.y -= 0.75;
  planetAmb = new THREE.AmbientLight(0xffffff, 0.2);
  planetAmb.position.y -= 12;
  scene.add(planetAmb);
  
}

function drawSats() {
  // draws the satellites orbiting around the planet.
  geo = new THREE.BoxGeometry();
  mat = new THREE.MeshStandardMaterial({
    color: 0x484848,
    //emissive: 0x444444,
    metalness: 0.7,
    roughness: 0.3,
    //shininess: 0.3,
    envMap: loader.load("assets/steel.tif"),
    flatShading: true
  });
  sat = new THREE.Mesh(geo, mat);
   
  sat.scale.set(1, 0.5, 0.5);
  
  sat.rotation.z -= 0.5;
  sat.rotation.y -= 1.3;
  sat.position.z += 4;
  sat.position.y -= 0.3;
  scene.add(sat);
  objects.push(sat);  
  //drawPanel();

  var satLightGeo = new THREE.SphereGeometry(1, 30, 30);
  var satLightMat = new THREE.MeshBasicMaterial({color: 0x0000ff});
  satLight = new THREE.Mesh(satLightGeo, satLightMat);
  satLight.position.z +=1;
  satLight.position.y += 0.6;
  satLight.position.z -= 1;
  sat.add(satLight); 
  satLight.scale.set(1, 2, 2);
  satLight.scale.set(0.05, 0.1, 0.1);  
  satlight = new THREE.PointLight(0x0000ff, 0.3);
  sat.add(satlight);

  geo = new THREE.DodecahedronGeometry();
  mat = new THREE.MeshPhysicalMaterial({
    clearcoat: 1.,
    clearcoatRoughness: 0.,
    color: 0x484848,
    side: THREE.DoubleSide,
    roughness: 0.
    });
  sat2 = new THREE.Mesh(geo, mat);
  sat2.position.z += 4;
  sat2.position.y -= 3.7;
  sat2.position.x += 2
  sat2.scale.set(0.3, 0.3, 0.3);
  scene.add(sat2);
  //junk.rotation.x +=1;
}

function drawMoon() {
  // Draws the moon orbiting the planet.
  var geo = new THREE.SphereGeometry(radius=1, widthSegments=32,heightSegments=32); 
  var mat = new THREE.MeshLambertMaterial({
    color: 0x777777,
    map: loader.load("assets/moon.jpg"),
    //bumpMap: loader.load("assets/ldem-3-8bit.jpg"),
    //bumpScale: 0.2,
    //roughness: 1,
    //shininess: 0

    });

  moon = new THREE.Mesh(geo, mat);
  //europa.scale.set(1.5,1.5,1.5);
  scene.add(moon);
  objects.push(moon);
  moon.position.z += 5;
  moon.position.y -= 1.5;
  
  moon.scale.set(0.65, 0.65, 0.65);
}

function drawStars() {
  // Draws the star off in the distance in the top left corner.
  geo = new THREE.SphereGeometry(radius=1.5, widthSegments=32,heightSegments=32); 
  mat = new THREE.MeshBasicMaterial({color: 0xF28C38 });
  sunlight = new THREE.DirectionalLight(0xffffff, 1);
  var sun = new THREE.Mesh(geo, mat);
  scene.add(sun);
  scene.add(sunlight);
  objects.push(sun);
  sun.position.z -= 2;
  sun.position.y += 1;
  sun.position.x -= 5;
  sun.scale.set(0.15,0.15,0.15);
  sunlight.position.x -= 5.1;
  sunlight.position.y += 0.5;
  sunlight.position.z -= 2;
  sunlight.target = planet;
  //var sunlightHelper = new THREE.DirectionalLightHelper(sunlight, 1, 0xffffff);
  //scene.add(sunlightHelper);

  

  geo = new THREE.SphereGeometry(radius=1.5, widthSegments=32,heightSegments=32); 
  mat = new THREE.MeshBasicMaterial({color: 0x871717 });
  star = new THREE.Mesh(geo, mat);
  scene.add(star);
  objects.push(star);
  star.position.z -= 3;
  star.position.y -= 7;
  star.position.x += 7;
  star.scale.set(0.07,0.07,0.07);

  starLight = new THREE.SpotLight(0xff0000, 0.7);
  starLight.position.set(star.position.x, star.position.y, star.position.z);
  scene.add(starLight);
  var starLightHelper = new THREE.SpotLightHelper(starLight, 0xffffff);
  //scene.add(starLightHelper);
}


function orbitX(object, speed, radius) {
  // originally from this stackoverflow thread - https://stackoverflow.com/questions/42418958/rotate-an-object-around-an-orbit
  // this specific answer - https://stackoverflow.com/a/42419603/13192204
  // makes an object rotate around the planet
  speed = Date.now() * speed;
  object.position.set(Math.cos(speed) * radius, -3, Math.sin(speed) * radius);
}

function orbitY(object, speed, radius) {
  // originally from this stackoverflow thread - https://stackoverflow.com/questions/42418958/rotate-an-object-around-an-orbit
  // this specific answer - https://stackoverflow.com/a/42419603/13192204
  // makes an object rotate around the planet
  speed = Date.now() * speed;
  object.position.set(0, Math.cos(speed) * radius - 3.5, Math.sin(speed) * radius + 0.5);
}

function orbit(object, speed, radius) {
  // originally from this stackoverflow thread - https://stackoverflow.com/questions/42418958/rotate-an-object-around-an-orbit
  // this specific answer - https://stackoverflow.com/a/42419603/13192204
  // makes an object rotate around the planet
  speed = Date.now() * speed;
  object.position.set(
    Math.cos(speed) * radius, 
    Math.cos(speed) * radius * -1 + 2,
     //-0.5, 
    //0,
    Math.sin(speed) * radius + 1);
}
init();
drawPlanet();

drawMoon();
drawStars();
//starLight.target = junk;
drawSats();
starLight.target = sat2;
var hl = new THREE.HemisphereLight(0xffffff, starLight.color, 0.1);
scene.add(hl);

//var al = new THREE.AmbientLight(0xffffff, 0.5);
//scene.add(al);
const animate = function () {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
  //sat.rotation.z += 0.01;
  planet.rotation.y += 0.01;
  orbitX(sat, -0.002, 3.5);
  orbit(sat2, -0.0004, 3.4)
  orbit(moon, -0.001, 4);
  //orbitY(plane, 0.001, 4);
  sat2.rotation.x += 0.04;
  sat2.rotation.y += 0.03;
  sat2.rotation.z += 0.03;
  moon.rotation.y += 0.04;
}


function onDocumentKeyDown(event) {

var keyCode = event.which;
    if (keyCode === 68) { //d
      console.log("toggling directional light");

      if (sunlight.intensity > 0) {
        sunlight.intensity = 0;
      } else {
        sunlight.intensity = 1;
      }
    } else if (keyCode === 72) { // h
         console.log("toggling hemisphere light");
         if (hl.intensity > 0) {
           hl.intensity = 0;
         } else {
           hl.intensity = 0.4;
         }
      } else if (keyCode === 83) { // s
          console.log("toggling spotlight");
          if (starLight.intensity > 0){
            starLight.intensity = 0;
          } else {
            starLight.intensity = 0.7;
          }
      } else if (keyCode === 65){ //a
          console.log("toggling ambient light");
          //console.log(planetAmb.intensity);
          if (planetAmb.intensity > 0) {
              planetAmb.intensity = 0;
          } else {
            planetAmb.intensity = 0.2;
          }
    } else if (keyCode === 80) { //p
        console.log("toggling point light");
        if (satlight.intensity > 0) {
          satlight.intensity = 0;
        } else {
          satlight.intensity = 0.3;
        }
    }
}

var planeGeo = new THREE.PlaneGeometry(3);
var planeMat = new THREE.MeshPhongMaterial({color: 0x2e3440});
var plane = new THREE.Mesh(planeGeo, planeMat);
plane.position.y -= 4;
plane.position.z += 2;
scene.add(plane);
//modify light stuff
animate();


