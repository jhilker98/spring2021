let camera, near, far;
let scene;
let renderer;
let geo, mat, mesh;
let light, lightHelper;
let ADD = 0.005,
  theta = 0;

const loader = new THREE.TextureLoader();
const lights = [];

const colors = {
  bg: 0x2e3440,
  fog: 0x4a5468,
  bubbles: 0x6f7d98,
  white: 0xffffff,
  blue: 0x5e81ac,
  red: 0xbf616a,
  purple: 0xb48ead,
  darkpurple: 0xb16286,
  coin: 0xf28c38,
};

var ground, pump;
var bubble;
const bubbles = [];
var shark, pufferfish, fish;
var coin;
var ball;
var lamp;

function init() {
  camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );

  camera.position.y = 7;
  camera.position.z = 75;

  near = 1;
  far = 100;
  scene = new THREE.Scene();
  scene.background = new THREE.Color(colors.bg);
//  scene.fog = new THREE.Fog(colors.fog, near, far);
  scene.fog = new THREE.FogExp2(colors.fog, 0.03);
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFShadowMap;
  document.addEventListener('keydown',toggleLights);
  document.body.appendChild(renderer.domElement);

  light = new THREE.AmbientLight(0xffffff, 0.2);
  scene.add(light);
  lights.push(light);

  light = new THREE.HemisphereLight(colors.purple, colors.purple, 0.2);
  scene.add(light);
  lights.push(light);
  }

function drawGround() {
  // Create the ground
  // Image courtousy to By Ji-Elle - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=9429566
  let texture = loader.load(
    "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Adrar_sands.JPG/1280px-Adrar_sands.JPG"
  );
  mat = new THREE.MeshLambertMaterial({ map: texture });
  geo = new THREE.BoxGeometry(1000, 1, 1000);
  ground = new THREE.Mesh(geo, mat);
  ground.position.y = -1;
  ground.receiveShadow = true;

  scene.add(ground);
}

function drawBubbles() {
        geo = new THREE.SphereGeometry();
        mat = new THREE.MeshBasicMaterial({
            color: colors.bubbles,
            transparent: true,
            opacity: 0.4
        });
        for (var i = 0; i < 10; i++) {
        bubble = new THREE.Mesh(geo, mat);
        bubble.position.x = Math.floor(Math.random() * (10 - 5 + 1) + 10); //The maximum is inclusive and the minimum is inclusive
        scene.add(bubble);
        bubbles.push(bubble);
        }
    }

function drawPump() {
    geo = new THREE.CylinderGeometry(1,1,5);
    mat = new THREE.MeshStandardMaterial({
        color: colors.white,
        metalness: 0.7,
        roughness: 0.3,
        //flatShading: true
    });

    pump = new THREE.Mesh(geo, mat);
    pump.rotation.z = 1.55;
    pump.position.x = 10;
    pump.position.z = 20;
    pump.scale.set(2,2,2);
    scene.add(pump);

    geo = new THREE.SphereGeometry(2);
    mat = new THREE.MeshBasicMaterial({
        color: colors.darkpurple,
    });
    var pumplight = new THREE.Mesh(geo, mat);
    pumplight.position.x += 1;
    pumplight.scale.set(0.35,0.35,0.35);
    pump.add(pumplight);

    light = new THREE.DirectionalLight(colors.purple, 0.4);
    light.position.y += 0.2;
    //light.target = coin;
    pumplight.add(light);
    lights.push(light);
}

function drawBall() {
  geo = new THREE.SphereGeometry(2, 32, 32);
  mat = new THREE.MeshPhysicalMaterial({
    color: colors.blue,
    metalness: 0,
//    shininess: 0,
    clearcoat: 0.7,
  });
  ball = new THREE.Mesh(geo, mat);

  ball.position.y = 1.85;
  ball.position.x = -15;
  ball.position.z = 40;
  //ball.scale.set(0.7, 0.6, 0.6);
  scene.add(ball);
}

function drawCoin() {
geo = new THREE.CylinderGeometry(1, 1, 0.5);
mat = new THREE.MeshPhysicalMaterial({
    color: colors.coin,
    metalness: 1
});
    coin = new THREE.Mesh(geo, mat);
    coin.position.x = 16;
    coin.position.y = 1.85;
    coin.position.z = 50;
    scene.add(coin);

}

function drawPufferfish() {

    geo = new THREE.SphereGeometry();
    mat = new THREE.MeshPhongMaterial({
        color: 'green',
    });

    pufferfish = new THREE.Mesh(geo, mat);
    pufferfish.position.y = 30;
    pufferfish.scale.set(1.7,1.7,1);
    scene.add(pufferfish);

    geo = new THREE.ConeGeometry();
    var fin = new THREE.Mesh(geo, mat);
    fin.scale.set(1.5,1.5,1.5);
    fin.position.x = -0.7;
    pufferfish.add(fin);

    pufferfish.scale.set(2,2,2);

}

function drawFish() {
    geo = new THREE.SphereGeometry();
    mat = new THREE.MeshPhongMaterial({
        color: 'grey',
    });

    fish = new THREE.Mesh(geo, mat);
    fish.position.y = 20;
    fish.scale.set(1.7,1.7,0.7);
    scene.add(fish);

    geo = new THREE.ConeGeometry();
    var fin = new THREE.Mesh(geo, mat);
    fin.scale.set(1.5,1.5,1.5);
    fin.position.x = -0.7;
    fish.add(fin);

}

function drawShark() {

    geo = new THREE.SphereGeometry();
    mat = new THREE.MeshPhongMaterial({
        color: 'lightgrey',
    });

    shark = new THREE.Mesh(geo, mat);
    shark.position.y = 40;
    shark.scale.set(1.7,1.7,1);
    scene.add(shark);

    geo = new THREE.ConeGeometry();
    var fin = new THREE.Mesh(geo, mat);
    fin.scale.set(1.5,1.5,1.5);
    fin.position.x = -0.7;
    shark.add(fin);

    shark.scale.set(2,2,2);

}

function toggleLights(event) {
    var keyCode = event.which;
    if (keyCode == 65) { // a, for ambient
        console.log("toggling ambient light");
        if (lights[0].intensity > 0) {
            lights[0].intensity = 0;
        } else {
            lights[0].intensity = 0.2;
        }
    } else if (keyCode == 72) { // h, for hemisphere
        console.log("toggling hemisphere light");
        if (lights[1].intensity > 0) {
            lights[1].intensity = 0;
        } else {
            lights[1].intensity = 0.2;
        }
    } else if (keyCode == 68) { // d, for directional
        console.log("toggling directional light");
        if (lights[2].intensity > 0) {
            lights[2].intensity = 0;
        } else {
            lights[2].intensity = 0.2;
        }
    }
}

init();
drawGround();
drawPump();
drawBall();
drawCoin();
drawBubbles();
drawPufferfish();
drawFish();
drawShark();

function animate() {
  renderer.render(scene, camera);
    var puff = true;
    //pump.rotation.z += 0.1;
    theta += ADD;
    for (bubble of bubbles) {
        bubble.position.y += 5 * Math.random();
        if (bubble.position.y > 70) {
            bubble.position.y = pump.position.y;
        }
    }

    fish.position.x += 3 * Math.random();
    if (fish.position.x > 140) {
        fish.position.x = -140;
    }

    shark.position.x += 5 * Math.random();
    if (shark.position.x > 140) {
        shark.position.x = -140;
    }

    pufferfish.position.x += 7 * Math.random();
    if (pufferfish.position.x > 140) {
        pufferfish.position.x = -140;
    }


    if (pufferfish.scale.x > 3) {
        puff = false;
    } else if (pufferfish.scale.x < 1) {
        puff = true;
    }


    if (puff == true) {
        pufferfish.scale.x += 0.1;
        pufferfish.scale.y += 0.1;
        pufferfish.scale.z += 0.1;
    } else {
        pufferfish.scale.x -= 0.1;
        pufferfish.scale.y -= 0.1;
        pufferfish.scale.z -= 0.1;
    }
    requestAnimationFrame(animate);
}

animate();
