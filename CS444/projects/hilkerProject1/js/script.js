/**
 * @file Draws a seascape at night, with a rotating buoy and a log raft floating by.
 * @author Jacob Hilker
 */

var camera;
var scene;
var renderer;
var geo, mat;
var sand;
var castle, roof;
var sea;
var moon, moonlight;
var light;
var lighthouse, lhLight;
var sunlight, sunset;
var car, wheel;
var comet;
var buoy, siren;
//var star;
var stars;
var bush;
var raft;
var shell;
var ball;

var lightHelper;
var moonVisible = true;
var lhBlink = false;

//var down = true;

const clouds = [];
const lights = [];

const FIRE_INTENSITY = 0.4;

var buoyBob = false;

var colors = {
  white: 0xffffff,
  black: 0x000000,
  //day: 0x5fa6c3,
  //night: 0x021f4b,
  night: 0x060708,
  sand: 0xc2b280,
  sun: 0xf28c38,
  sea: 0x70bdf2,
  lightsteel: 0xc1c3c4,
  steel: 0x43464b,
  buoy: 0xad3e43,
  light: 0xf8d568,
  sunsetClouds: [0xffe577, 0xfec051, 0xff8967, 0xfd6051],
  stars: [0xf28c38, 0xffdda7, 0xe9cff1, 0x888888, 0x4682b4],
  fog: 'lightgrey',
  comet: {
    color: 0x282828,
    emissive: 0x282828,
  },
  raft: 0x43281f,
  shell: 0xf28085,
  ball: 0x458588,
  bush: 0x7cfc00
};

const loader = new THREE.TextureLoader();
const objects = [];
var TIME = 0;

//console.log(colors.sunsetClouds);

function init() {
  // Initializes a scene.
  // Also adds in an ambient light.
  camera = new THREE.PerspectiveCamera(
    90,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );
  scene = new THREE.Scene();
  //scene.fog = new THREE.Fog(colors.white, 10, -60);
  //scene.fog = new THREE.FogExp2(new THREE.Color(colors.white), 0.04);
  //scene.background = new THREE.Color(colors.day);
  scene.background = new THREE.Color(colors.night);
  renderer = new THREE.WebGLRenderer({ antialias: true });
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  //camera.position.x = 1;
  camera.position.z = 6;
  camera.rotation.y = -0.21;

  light = new THREE.AmbientLight(colors.white, 0.2);
  scene.add(light);
  lights.push(light);

  document.addEventListener('keydown', toggleLight);
}

function drawMoon() {
  geo = new THREE.SphereGeometry(1.5, 32, 32);
  mat = new THREE.MeshBasicMaterial({
    //color: colors.sun,
    color: 0xffffff,
    map: loader.load('assets/moon.jpg'),
  });
  moon = new THREE.Mesh(geo, mat);
  moon.position.y = 2;
  moon.position.z = -3;
  moon.scale.set(0.35, 0.35, 0.15);
  scene.add(moon);
  objects.push(moon);

  moonlight = new THREE.PointLight(0xffffff, 0.3);
  moonlight.castShadow = true;
  //moonlight.position.y = 4;
  //moonlight.position.z = -2.3;
  moon.add(moonlight);
  lights.push(light);

}

function drawSea() {
  geo = new THREE.PlaneGeometry(75, 5, 15);
  mat = new THREE.MeshPhysicalMaterial({
    color: colors.sea,
    clearcoat: 1,
    clearcoatMap: loader.load('assets/waternormals.jpg'),
    clearcoatRoughness: 0.4,
    //normalMap: loader.load('assets/waternormals.jpg'),
    ior: 2.333,
  });
  sea = new THREE.Mesh(geo, mat);
  sea.position.y = -1;
  //sea.position.x
  sea.rotation.x = -1.25;
  scene.add(sea);
  objects.push(sea);
}

function drawSand() {
  geo = new THREE.PlaneGeometry(75, 8, 8);
  mat = new THREE.MeshLambertMaterial({
    color: colors.sand,

    specularMap: loader.load('assets/sand-002-spec.jpg'),
    //envMap: loader.load('assets/sand-002-nrm.jpg'),
    //map: loader.load("assets/sand-002-color.jpg")
  });
  sand = new THREE.Mesh(geo, mat);
  sand.position.y = -5;
  sand.rotation.x = -1.1;

  scene.add(sand);
  objects.push(sand);
}

function drawLighthouse() {
  geo = new THREE.CylinderGeometry(0.2, 0.3, 4);
  mat = new THREE.MeshStandardMaterial({
    emissive: colors.steel,
    color: colors.steel,
    // specular: 0xff0000,
    envMap: loader.load('assets/steel.tif'),
    metalness: 0.75,
    roughness: 0.4,
    refractionRatio: 0.6,
  });
  lighthouse = new THREE.Mesh(geo, mat);
  lighthouse.position.z = -3;
  lighthouse.position.x = -11;
  scene.add(lighthouse);
  objects.push(lighthouse);

  geo = new THREE.CylinderGeometry(0.2, 0.2, 0.3);
  mat = new THREE.MeshBasicMaterial({ color: colors.light });
  lhLight = new THREE.Mesh(geo, mat);
  lhLight.position.y = 2;
  lighthouse.add(lhLight);

  light = new THREE.SpotLight(colors.light, 0.4, 0);
  lhLight.add(light);
  lights.push(light);

  //lightHelper = new THREE.SpotLightHelper(light, colors.white);
  //scene.add(lightHelper);

  geo = new THREE.SphereGeometry(0.2, 30, 30);
  mat = new THREE.MeshBasicMaterial({ color: 0xff0000 });

  geo = new THREE.ConeGeometry(0.2, 0.5);
  mat = new THREE.MeshStandardMaterial({
    emissive: colors.steel,
    color: colors.steel,
    // specular: 0xff0000,
    envMap: loader.load('assets/steel.tif'),
    metalness: 0.75,
    roughness: 0.4,
    refractionRatio: 0.6,
  });
  var top = new THREE.Mesh(geo, mat);
  top.position.y += 0.4;
  lhLight.add(top);
}

function drawBuoy() {
  geo = new THREE.TorusGeometry(0.5, 0.35);
  mat = new THREE.MeshPhongMaterial({
    color: colors.buoy,
  });
  buoy = new THREE.Mesh(geo, mat);
  buoy.rotation.x = 1.5;
  buoy.scale.set(0.5, 0.5, 0.5);
  buoy.position.z = -0.5;
  buoy.position.y = -0.85;
  scene.add(buoy);
  objects.push(buoy);

  geo = new THREE.SphereGeometry(0.2);
  mat = new THREE.MeshBasicMaterial({
    color: 0x00ffff,
    transparent: true,
    opacity: 0.3,
  });

  siren = new THREE.Mesh(geo, mat);
  //siren.scale.set(0.3,0.3,0.3);
  siren.position.z = -0.35;
  // siren.position.y -= 0.01;
  buoy.add(siren);
  light = new THREE.DirectionalLight(0x00ffff, 0.15);
  light.position.x = -0.7;
  light.position.y -= 2;

  siren.add(light);
  lights.push(light);

  light = new THREE.PointLight(0x00ffff, 0.15);
  light.position.x = -0.7;
  light.position.y -= 2;
  siren.add(light);
  //lightHelper = new THREE.PointLightHelper(light, 2, colors.white);
  // scene.add(lightHelper);
}

function drawStars(starColor) {
  /* Draws the stars in the sky.
   * Params: starColor: hex number. Part of the stars array in the colors object.
   * Originally from the PointsMaterial Example in the ThreeJS docs.
   * https://threejs.org/docs/#api/en/materials/PointsMaterial
   */
  var verts = [];
  for (let i = 0; i < 4000; i++) {
    const x = THREE.MathUtils.randFloatSpread(2000);
    const y = THREE.MathUtils.randFloatSpread(2000);
    const z = THREE.MathUtils.randFloatSpread(2000);
    verts.push(x, y, z);
  }

  geo = new THREE.BufferGeometry();
  geo.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3));
  mat = new THREE.PointsMaterial({
    color: starColor,
  });

  stars = new THREE.Points(geo, mat);

  scene.add(stars);
  console.log(stars.opacity);
}

function drawCastle() {
  /*
   * Draws the WIP sand castle, and bucket next to it.
   */
  geo = new THREE.CylinderGeometry(0.3, 0.5, 1);
  mat = new THREE.MeshLambertMaterial({
    color: colors.sand,

    //specularMap: loader.load('assets/sand-002-spec.jpg'),
    //envMap: loader.load('assets/sand-002-nrm.jpg'),
    //map: loader.load("assets/sand-002-color.jpg")
  });

  castle = new THREE.Mesh(geo, mat);
  castle.position.z = 3.2;
  castle.position.y = -1;
  castle.scale.set(0.75, 0.75, 0.75);
  scene.add(castle);
  objects.push(castle);
}

function drawBush() {
  /*
   * Draws the bush in the scene.
   */

  geo = new THREE.DodecahedronGeometry(0.35);
  mat = new THREE.MeshPhysicalMaterial({
    color: colors.bush,
    metalness: 0,
   // shininess: 0,
   // clearcoat: 0.7,
  });
  bush = new THREE.Mesh(geo, mat);

  bush.position.y = -0.85;
  //bush.position.x = 1.5;
  bush.position.z = 5.2;
  bush.scale.set(0.7, 0.6, 0.6);
  scene.add(bush);

  var bush2 = bush.clone();
  bush2.position.x = 0.35;
  scene.add(bush2);
  var bush3 = bush.clone();
  bush3.position.x = - 0.35;
  scene.add(bush3);
}

function drawRaft() {
  /*
   * Draws the raft.
   */

  geo = new THREE.BoxGeometry(1, 0.5, 3);
  mat = new THREE.MeshPhongMaterial({
    color: colors.raft,
    bumpMap: loader.load('assets/wood-height.png'),
    normalMap: loader.load('assets/wood-normal.png'),
    //combine: THREE.MixOperation,
    bumpScale: 0.2,
  });
  raft = new THREE.Mesh(geo, mat);
  raft.rotation.y = 1.575;
  raft.rotation.z = 0.15;
  raft.position.z = -1;
  raft.position.y = -0.25;
  scene.add(raft);
}

function drawBall() {
  /*
   * Draws the ball on the beach
   */
  geo = new THREE.SphereGeometry(0.35, 32, 20);
  mat = new THREE.MeshPhysicalMaterial({
    color: colors.ball,
    metalness: 0,
    shininess: 0,
    clearcoat: 0.7,
  });
  ball = new THREE.Mesh(geo, mat);

  ball.position.y = -0.85;
  ball.position.x = 1.5;
  ball.position.z = 4.5;
  ball.scale.set(0.7, 0.6, 0.6);
  scene.add(ball);
}

init();
drawSea();
drawMoon();
drawSand();
drawLighthouse();
drawBuoy();
drawCastle();
drawBall();
for (var color of colors.stars) {
  drawStars(color);
}
drawStars(colors.white);
drawRaft();
drawBush();
function animateMoon() {
  /*
   * Makes the moon rise and fall.
   */
  moon.position.x = 5 * Math.sin(TIME) + 0;
  moon.position.y = 6 * Math.cos(TIME) - 0;
}

function moveBuoy() {
  /*
   * Moves the buoy back and forth and makes it bob up and down.
   */
  buoy.position.x += 0.01;
  buoy.rotation.z += 0.03;

  if (buoy.position.y < -0.95) {
    buoyBob = true;
  } else if (buoy.position.y > -0.85) {
    buoyBob = false;
  }
  if (buoyBob == true) {
    buoy.position.y += 0.004;
  } else {
    buoy.position.y -= 0.004;
  }

  if (buoy.position.x > 35) {
    buoy.position.x = -27;
  }
}

function moveRaft() {
  /*
   * Moves the boat and rocks it.
   */

  raft.position.x += 0.02;
  if (raft.position.x > 35) {
    raft.position.x = -27;
  }
  //if (boat.rotation.)
}

const animate = function () {
  TIME += 0.004;
  requestAnimationFrame(animate);

  animateMoon();
  moveBuoy();
  moveRaft();

  if (moon.position.y >= 0) {
    lhBlink = false;
  } else {
    lhBlink = true;
  }
  ball.translateX(-0.03);
  ball.rotation.x += 0.3;
  if (ball.position.x < -35) {
    ball.position.x = 35;
  }

  
  renderer.render(scene, camera);
};

function toggleLight(event) {
  var keyCode = event.which;
   if (keyCode === 65) { // a
    console.log('toggling ambient light');

    if (lights[0].intensity > 0) {
      lights[0].intensity = 0;
    } else {
      lights[0].intensity = 0.15;
    }
  } else if (keyCode === 80) { // p
    if (moon.children[0].intensity > 0) {
      moon.children[0].intensity = 0;
    } else {
      moon.children[0].intensity = 0.3;
    }

    // old example - used to show the spinning light
    //    if (siren.children[1].intensity > 0) {
    //   siren.children[1].intensity = 0;
    // } else {
    //   siren.children[1].intensity = 0.15;
    // }

  } else if (keyCode === 68) { // d
 
    console.log('toggling directional light - siren lights')
    if (siren.children[0].intensity > 0) {
      siren.children[0].intensity = 0;
    } else {
      siren.children[0].intensity = 0.15;
    }

     if (siren.children[1].intensity > 0) {
      siren.children[1].intensity = 0;
    } else {
      siren.children[1].intensity = 0.15;
    }
  } else if (keyCode === 83) { // s
    if (lhLight.children[0].intensity > 0) {
      lhLight.children[0].intensity = 0;
    } else {
      lhLight.children[0].intensity = 0.4;
    } 
  }
}
animate();
