#+title: Homework 1 - Simple Web Server
#+author: Jacob Hilker

* Part One - =localhost=
** Figuring out My IP Address
[[local_ipconfig.png]]

** Running The Server
*** With The Correct File
[[local_html.png]]

*** With a Non-Existant File
[[local_404.png]]

* Part Two - On The Virtual Machines
** Figuring Out The IP Addresses
[[server_ipconfig.png]]
[[client_ipconfig.png]]