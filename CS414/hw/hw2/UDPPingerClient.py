# Jacob Hilker
# I hereby declare upon my word of honor that I have neither given nor received unauthorized help on this work.

import sys, time
from socket import *

# Get the server hostname and port as command line arguments
argv = sys.argv     
host = argv[1]
port = argv[2]
timeout = 1 # timeout variable in second
 
# TODO-Create UDP client socket
# Note to make use of IP_v4 and UDP datagram packet
clientSocket = socket(AF_INET, SOCK_DGRAM)
# TODO-Set clientSocket timeout as one second using settimeout() method. You must use the 
#timeout variable created earlier to set this value. 
clientSocket.settimeout(timeout)
# Command line argument is a string, change the port into integer
port = int(port)  
# Sequence number of the ping message
ptime = 0  

# Ping the server for 10 times
while ptime < 10: 
    ptime += 1
    # Create and format the UDP data packet or ping message to be sent to the server
    data = "Ping " + str(ptime) + " " + time.asctime()
    
    try:
	# Sent time
        RTTb = time.time()
	# TODO-Send the encoded UDP data packet or the ping message created earlier after encoding 
	# to the specified IP address-host and port number (these are accepted as command line arguments).
	# Use sendto() method to send data to server. The first argument of sendto() will be 
	#the ping message, and second argument will contain server IP and port number. 
        clientSocket.sendto(str.encode(data),(host, port))
	# TODO-Receive the server response and store it into variables named message and address
        message, address = clientSocket.recvfrom(1024)
    #convert received message into a string
        message = str(message)      
	# Received time
        RTTa = time.time()
	# TODO-Display or print the server response (both the address and message) 
        print(f"message is {message}")    
	# TODO-print the Round trip time. Subtract sent time from received time.
        print(RTTa - RTTb)
	#Encapsulate the subtracted time as string before printing it using str() method.
        
    except:
        # Server does not response
	# Assume the packet is lost
        print("Request timed out.")
        continue

# TODO-Close the client socket
clientSocket.close()
