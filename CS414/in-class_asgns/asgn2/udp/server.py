from socket import *
import datetime
serverPort = 12000
serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind(('', serverPort))
print("The server is ready to receive")
while 1:
	message, clientAddress = serverSocket.recvfrom(2048)

	modifiedMessage = str.encode(datetime.datetime.now().strftime("%A, %B %d, %Y, %I:%M %p\n"))

	serverSocket.sendto(modifiedMessage, clientAddress)
