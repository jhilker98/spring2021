#Let's do an example of raw sockets
#Before doing this example see the video on raw sockets

#We need a few new imports for this type of socket programming
#The normal socket stuff
from socket import *

#We are going to have to interact with the OS more as we take
#over some of the jobs that the TCP/IP stack has done for use previously
import os
import sys

#We need to create some structs that will be our headers
import struct

#We will send a payload which include the time we sent the packet
import time

#we need to do some conversions
import select
import binascii  

def checksumf(stri: str): 
	csum = 0
	countTo = (len(stri) / 2) * 2
	
	count = 0
	while count < countTo:
		thisVal = ord(chr(stri[count+1])) * 256 + ord(chr(stri[count])) 
		csum = csum + thisVal 
		csum = csum & 0xffffffff  
		count = count + 2
	
	if countTo < len(stri):
		csum = csum + ord(stri[len(stri) - 1])
		csum = csum & 0xffffffff
	
	csum = (csum >> 16) + (csum & 0xffff)
	csum = csum + (csum >> 16)
	answer = ~csum 
	answer = answer & 0xffff 
	answer = answer >> 8 | (answer << 8 & 0xff00)
	return answer 



def example1(test: str):
	print(test)
	#For this example we are going to create our own ICMP packets via raw sockets.
	#Recall that this means we are responsible for all the stack responsibilities 
	#from the ICMP protocol and up. But, this will give us complete control
	#over the contents of the ICMP header and allow us to modify things like
	#the TTL, which we wouldn't be able to do if the OS was creating the header.
	#For example, if we used raw sockets to create an IP packet, we would be
	#in control of everything from the IP level an up! This would allow us to 
	#do things like spoof IP addresses. We will come back to that later, for 
	#now lets play with the ICMP protocol. 
	
	#We fist need to create the socket. This is a bit different for raw
	#sockets.
	
	#First we will need to tell the raw socket what level/protocol 
	#we will start and be responsible for. 
	#The function getprotobyname() takes a protocol string like 
	#"TCP", "UDP" or "ICMP" and returns the associated constant for the 
	#protocol as defined by the socket module.
	
	#So let's get the constant associated with ICMP
	icmp = getprotobyname("icmp")
	
	#Now we create the socket using the normal socket() function.
	#We still want a TCP/IP type network socket, so we use "AF_INET".
	#However, we now want a raw socket so instead of using something
	#like a "SOCK_DGRAM" we use "SOCK_RAW". Finally, we pass in the 
	#ICMP constant we got from the above function call. 
	mySocket = socket(AF_INET, SOCK_RAW, icmp) 
	
	#Let's use this raw socket to send a ping request with the TTL set to 
	#1. To do this, we need to construct the ICMP header. Therefore, we
	#need to know to format of the ICMP header. Luckily ICMP is not too 
	#difficult, more detailed info on the header format can be found 
	#here: https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol
	
	#We also need to pack this into a struct format that the OS will 
	#recognize, this means we need to use the python struct.pack()
	
	#Ok first we need to select a system to send this packet.
	#Note if we set the TTL=1, it won't reach this system but that is OK
	#we just need a target. To get the actual address, use gethostbyname()
	destAddr = gethostbyname("google.com")
	
	#Recall that if we are handling everything from the transport level up,
	#We don't have a port number! So, we have to handle getting the 
	#packet to the correct process. To get the ID of the current 
	#process, and the process which we want to get info to and from, 
	#use the getpid() function.
	my_proc_ID = os.getpid() & 0xFFFF 
	
	#Ok now let's start assembling the header. For this review the 
	#ICMP header info; we need an 8-bit type, 8-bit code, 16-bit checksum,
	#16-bit ID, and 16-bit Sequence number.
	
	#Let's start computing the checksum. This is a bit weird because 
	#we have to computer the packet info, use it to get the checksum,
	#then recompute the packet info with the correct checksum. These
	#types of things are why we wouldn't use raw sockets unless we 
	#really needed them
	
	#First a dummy header
	myChecksum = 0
	
	#Type is an ICMP echo request, ping, which is 8
	ICMP_ECHO_REQUEST = 8
	#There is just one Code for a type=8 ICMP packet, 0
	ICMP_CODE = 0
	
	#We will just use a sequence number of 1
	
	# Now we need to make a dummy header with the 0 checksum
	# For this we have to use structs, 
	# Struct, Interpret strings as packed binary data.
	# But recall that we need to make sure each part of the header
	# is the correct length. For example, we can't use 16-bits for the 
	# ICMP type, it needs to be 8-bits. 
	# For this task we use struct.pack() to create the binary struct.
	# More info on this function here: https://docs.python.org/3/library/struct.html 
	
	#Format Strings:
	#The format string of struct.pack tells the function to "pack" each 
	#of the corresponding objects in a specific way. The end result is 
	#a byte object. 
	#We have 5 things which we need to pack, so our format string will 
	#contain 5 characters, each one is a code on how to pack the 
	#corresponding argument to the function.
	#Our format string is "bbHHh". Here is what each of those means:
	# 'b' -- signed char or 8 bits, so exactly what we need for the Code and Type
	# 'H' -- unsigned short or 16 bites, so what we need for checksum and ID
	# 'h' -- short so actually the same size but since it's a number we use the regular short.
	# Using the above we can now create our header:
	header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, ICMP_CODE, myChecksum, my_proc_ID, 1)
	
	#Now we need some data, let's just get the current time
	data = struct.pack("d", time.time())
	
	#Ok now we have the header and data we can calculate the actual 
	#checksum. There is a function below to do this according to the 
	#ICMP specifications. 
	# Calculate the checksum on the data and the dummy header.
	myChecksum = checksumf(header + data)
	
	#OK now we need to make sure the checksum is in the correct byte order
	#We can use the python function htons() to do this:
	#socket.htons(x) --Convert 16-bit positive integers from host to 
	#network byte order. 
	#On machines where the host byte order is the same as network byte order, 
	#this is a no-op; otherwise, it performs a 2-byte swap operation.
	myChecksum = htons(myChecksum)
	
	#Ok now we have the correct checksum, finally!
	#Lets reconstruct the header and pack in this new checksum
	header = struct.pack("bbHHh", ICMP_ECHO_REQUEST, 0, myChecksum, my_proc_ID, 1)
	
	#And finally create our packet
	packet = header + data
	
	# Now let's send the packet
	# AF_INET address must be tuple, not str
	# Both LISTS and TUPLES consist of a number of objects
	# which can be referenced by their position number within the object.
	mySocket.sendto(packet, (destAddr, 1)) 
	
	#OK now let's do the reverse and get the return info from this 
	#Ping request.
	
	#First get the response as normal 
	recPacket, addr = mySocket.recvfrom(1024)
	
	#Ok let's pull some parts out of this header
	# Fetch the ICMPHeader from the IP packet, so we only want the 
	# ICMP part. Notice that this step was done by the transport layer before
	# now since we are using raw packets we have to do it. 
	# the header, at least the TYPE, CODE, checksum, ID, and sequence number
	# are located between and including bytes 20 and 28
	icmpHeader = recPacket[20:28]
	
	#Now we need to unpack them just like we packet them above.
	#We can do this with struct.unpack().
	#It works just like struct.pack() but in reverse.
	icmpType, code, checksum, packetID, sequence = struct.unpack("bbHHh", icmpHeader)
	
	#Let see whats in these:
	print("Type: " + str(icmpType))
	print("Code: " + str(code))
	print("Squence: " + str(sequence))
	
	#Now lets say we also wanted to mess with the TTL
	#Let unpack it
	test = recPacket[8].to_bytes(1, byteorder="little")
	rawTTL = struct.unpack("s", test)[0]  
	# binascii -- Convert between binary and ASCII  
	TTL = int(binascii.hexlify(rawTTL), 16)
	print(TTL)
	
	return 0

example1("test")

